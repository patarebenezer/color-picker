export const color = [
    {
        kodewarna: 'bg-yellow-500',
        title: 'Yellow'
    },
    {
        kodewarna: 'bg-gray-800',
        title: 'Black'
    },
    {
        kodewarna: 'bg-red-700',
        title: 'Red'
    },
    {
        kodewarna: 'bg-green-600',
        title: 'Green'
    },
]