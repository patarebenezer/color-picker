import './App.css';
import { color } from './color';
import { useState } from 'react';
import GetData from './GetData';

function App() {
  const [selectedColor, setSelectedColor] = useState('bg-gray-100');
  const [nameColor, setNameColor] = useState('')
  const handleChangeColor = (value) => {
    setNameColor(value.title)
    setSelectedColor(value.kodewarna)
  }
  return (
    <div className='flex flex-col my-5 lg:flex-row w-full gap-4'>
      <div className='grid w-full justify-center border'>
        <div className={`w-full h-[400px] flex items-center justify-center text-2xl uppercase font-bold text-white ${selectedColor}`}>{nameColor}</div>
        <div className="flex gap-4 mt-4 mb-9">
          {
            color.map((value) => (
              <div
                className={`text-white text-center text-sm px-5 py-8 cursor-pointer hover:rounded-md w-full ${value.kodewarna}`}
                onClick={() => handleChangeColor(value)}
              >
                {value.kodewarna}
              </div>
            ))
          }
        </div>
      </div>

      <GetData />
    </div>
  );
}

export default App;
