import { useEffect, useState } from "react"
import axios from 'axios'
import reactStringReplace from 'react-string-replace';

const GetData = () => {
    const [data, setData] = useState([]);
    const [keyword, setKeyword] = useState('all');
    const url = `https://jsonmock.hackerrank.com/api/weather/search?name=${keyword}`
    const handleGetData = () => {
        axios.get(url).then((res) => {
            res.data.data.map((value) => (
                setData(value)
            ))
        })
    }
    useEffect(() => {
        handleGetData()
    }, [])
    return (
        <div className="lg:w-full border">
            <h1 className="text-center text-2xl font-bold text-gray-700 mb-5 mt-8">Get Data From API with Keywod</h1>
            <div className="hide text-center w-1/2 py-2 mx-auto mb-8">
                <p className="font-light mt-4">{data.name}</p>
                {
                    reactStringReplace(data.status, /(\d+)/g, (match, i) => (
                        <span className="font-light" key={i}>{match}, </span>
                    ))
                }
                {
                    reactStringReplace(data.weather, /(\d+)/g, (match, i) => (
                        <span className="font-light" key={i}>{match[0]}</span>
                    ))
                }
            </div>
            <div className="grid gap-4">
                <input placeholder="Type : all or oi | The default call api is all" type="text" className="px-4 border mx-auto rounded py-2 w-9/12" onChange={(e) => { setKeyword(e.target.value) }} autoFocus />
                <button type="submit" className="bg-blue-300 mx-auto py-2 rounded text-white w-1/4 hover:bg-blue-500" onClick={handleGetData}>Search</button>
            </div>
        </div>
    )
}

export default GetData